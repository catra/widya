from django.urls import path
from eduWeb import views

urlpatterns = [
    path('', views.widya, name='widya'),
    path('widya-2', views.widya2, name='widya-2')
]